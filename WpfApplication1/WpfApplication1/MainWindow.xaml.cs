﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            List<string> hashtags = new List<string>();
            List<string> mentions = new List<string>();
            List<string> quarantine = new List<string>();
            Message message1 = new Message();
            //Strings to store the values from the textboxes in.
            string mSender;
            string mMessage;

            if (string.IsNullOrWhiteSpace(txtSender.Text))
            {
                MessageBox.Show("Please Enter A Sender");
                mSender = "N/A";
            }
            else
            {
                mSender = txtSender.Text; // Sets the value of the header to the textbox value.
 
            }

            if (string.IsNullOrWhiteSpace(txtContent.Text))
            {
                MessageBox.Show("Please Enter A message");
                mMessage = "N/A";
            }
            else
            {
                mMessage = txtContent.Text;  //Sets the value of the message to the textbox value.

            }

            //message1.setmType(mSender);
            //message1.setmBody(mMessage);
            //Console.WriteLine(message1.mBody);
            //Console.WriteLine(message1.mType);
            string firstLetter = Convert.ToString(mSender[0]);
            string[] words = mMessage.Split(' ');
            List<string> Words = new List<string>(words);
            string Body;
            string Sender = Words[0];
            string Subject = Words[1];
            
            //Console.WriteLine(firstLetter);
            //Switch Statement to figure out the type of the Message
            switch (firstLetter)
            {
                case "S":
                    Text text1 = new Text(); //If its a text it nulls everything
                    text1.nullify();
                    text1.setmType(mSender);
                    Words.RemoveAt(0); // removes the first word from the body as its the sender not part of the message
                    string[] bodyArray = Words.ToArray();
                    Body = String.Join(" ", bodyArray);
                    //text1.setsBody(Body);
                    text1.setsSender(Sender); //checks for text speak
                    string Message = text1.textspeak(Body);
                    //Console.WriteLine(Message);
                    text1.setsBody(Message);
                    break;
                case "T":
                    Tweet tweet1 = new Tweet();
                    tweet1.nullify(); //nullifies all the values
                    Words.RemoveAt(0); // removes the first word as its the sender not the message
                    string[] BodyArray = Words.ToArray();
                    tweet1.settSender(Sender);
                    Body = String.Join(" ", BodyArray);
                    string message = tweet1.textspeak(Body);
                    tweet1.settBody(message); //sets the message
                    hashtags = tweet1.hashtags(BodyArray, hashtags); //checks for hashtags
                    mentions = tweet1.mentions(BodyArray, mentions); // checks for messages
                    break;
                case "E":
                    Email email1 = new Email(); //creates new emails
                    email1.nullify(); //nulls all the values
                    email1.seteBody(mMessage); // sets the message values
                    email1.seteSender(Sender); //set the sender value
                    if (Subject.Length < 21) //checks the subject length
                    {
                        if (Subject == "SIR") //checks to see if its a SIR
                        {
                            SIR sir1 = new SIR();
                            sir1.seteSubject(Subject);
                            Words.RemoveAt(1);
                            string sortCode = Words[1];
                            Words.RemoveAt(1);
                            string nature = Words[1];
                            Words.RemoveAt(1);
                            sir1.setiReport(nature);
                            sir1.setiCode(sortCode);
                        }
                        else
                        {
                            email1.seteSubject(Subject);
                            Words.RemoveAt(1);
                        }
                       
                    }
                    else
                    {
                        MessageBox.Show("Invalid Subject please enter a subject that is 20 characters or lower.");
                    }
                    string[] MessageArray = Words.ToArray();
                    quarantine = email1.quarantine(MessageArray, quarantine); //Quarantines the Hyperlinks
                    break;
                    default:
                    MessageBox.Show("Invalid Header ID Please Enter a valid One"); 
                    break;

            }    
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //Button To Clear the text Boxes.
            txtContent.Clear();
            txtSender.Clear();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //Button To End the program.
            this.Close();
        }
    }
}
