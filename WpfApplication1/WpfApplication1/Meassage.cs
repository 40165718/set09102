﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows;
using System.Text.RegularExpressions;

namespace WpfApplication1
{
    class Message
    {
        //M stands for Message
        //s stands for SMS
        //e stands for email
        //t stands for tweet
        public string mType;
        public string mBody;
        public string sBody;
        public string sSender;
        public string eSender;
        public string eSubject;
        public string eBody;
        public string tSender;
        public string tBody;

        public Message()
        {
            mType = "N/A";
            mBody = "N/A";
            sBody = "N/A";
            sSender = "N/A";
            eSender = "N/A";
            eSubject = "N/A";
            eBody = "N/A";
            tSender = "N/A";
            tBody = "N/A";
        }

        public Message(string nt, string nB)
        {
            mType = nt;
            mBody = nB;
        }
        //method to set the type/header
        public void setmType(string Type)
        {
            if (Type.Length == 10)
            {
                mType = Type;
            }
            else
            {
                MessageBox.Show("You Have entered an Invalid ID. Please Try Again.");
            }
        }
        //method to set the body
        public void setmBody(string Body)
        {
            mBody = Body;
        }
        //methodd to null all the values
        public void nullify()
        {
            sBody = null;
            sSender = null;
            eBody = null;
            eSender = null;
            eSubject = null;
            tSender = null;
            tBody = null;
        }
        //method to check for text speak
        public string textspeak(string body)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>(); //creates a new dictionary
            var reader = new StreamReader(File.OpenRead(@"C:\Users\Alex\Documents\SET09102CSV\textwords.csv")); //fills the dictionary from the csv file
            while (!reader.EndOfStream)
            {
                var lines = reader.ReadLine(); 
                var values = lines.Split(',');

                dict.Add(values[0], values[1]);
            }

            var words = body.Split(' ', ',');
            var sb = new StringBuilder();
            foreach (var word in words)
            {
                if (dict.ContainsKey(word)) //checks to see if the words match the dictionary
                {
                    if (dict[word] != null)
                    {
                        sb.Append(String.Format(" <{0}> ", dict[word])); //if it does changes the word.
                    }
                    else
                    {
                        sb.Append(word + " "); //otherwise nothing is changed
                    }
                }
                else
                {
                    sb.Append(word + " ");
                }
            }
            return sb.ToString().TrimEnd(' ');
        }
    }

    class Text : Message
    {
        public Text()
        {

        }
        public Text (string nS, string nBD)
        {
            sBody = nBD;
            sSender = nS;
        }
        //method to set the body
        public void setsBody(string Body)
        {
            if(Body.Length < 141 && Body.Length > 0) //checks the length
            {
                sBody = Body;
            }
            else
            {
                MessageBox.Show("Invalid Message, Message Too long Please try again");
            }
        }

        
        public void setsSender(string Sender)
        {
            double check;
            bool isNumeric = double.TryParse(Sender, out check); //checks to see if its numeric
            //Console.WriteLine(check);
            if(isNumeric == true && Sender.Length > 10 && Sender.Length < 12) //checks length and whether its numeric
            {
                sSender = Sender;
            }
            else
            {
                MessageBox.Show("You have not entered a valid phone Number Please Try Again");
            }
        }

    }

    class Email : Message
    {
        public Email()
        {

        }
        public Email(string nS, string nBD, string nSJ)
        {
            eBody = nBD;
            eSender = nS;
            eSubject = nSJ;
        }

        public void seteBody(string Body)
        {
            if(Body.Length > 1028) //checks body length
            {
                MessageBox.Show("Invalid Message. Message Too Long.");
            }
            else
            {
                eBody = Body;
            }
        }

        public void seteSender(string Sender)
        {
            if(Sender == "SIR")
            {
                eSender = Sender;
                MessageBox.Show("Incident Report Created");
            }
            eSender = Sender;
        }

        public void seteSubject(string Subject)
        {
            if (Subject.Length > 20) //checks subject length
            {
                MessageBox.Show("Invalid Subject. Subject Too Long.");
            }
            else
            {
                eSubject = Subject;
            }
        }
        public List<string> quarantine(string[] body, List<string> quarantine)
        {

            foreach(string s in body)
            {
                if(s.Contains(".co.uk") || s.Contains(".com") || s.Contains(".edu") || s.Contains(".gov")) //checks for hypperlinks
                {
                    quarantine.Add(s);
                    MessageBox.Show(s + ". This link was quarantined.");
                }
            }
            return quarantine;
        }
    }
    class Tweet : Message
    {
        public Tweet()
        {

        }
        public Tweet(string nS, string nBD)
        {
            tBody = nBD;
            tSender = nS;
        }

        public void settBody(string Body)
        {
            if  (Body.Length > 140) //checks length of tweet
            {
                MessageBox.Show("Invalid Tweet. Tweet Too Long.");
            }
            else
            {
                tBody = Body;
            }
        }

        public void settSender(string Sender)
        {
            if (Sender.StartsWith("@") && Sender.Length>0 && Sender.Length < 17) //chek validity of name
            {
                tSender = Sender;
            }
            else
            {
                MessageBox.Show("Invalid Twitter Handle. Please Try Again");
            }
        }

        public List<String> hashtags(string[] body, List<String> hashtags)
        {
            foreach(string s in body)
            {
                if (s.StartsWith("#")) //checks for hastags
                {
                    hashtags.Add(s);
                    MessageBox.Show(s + " Was used in this message");
                }
            }
            return hashtags;        
        }
        public List<string> mentions(string[] body, List<string> mentions)
        {
            foreach(string s in body)
            {
                if (s.StartsWith("@")) //checks for mentuions
                {
                    mentions.Add(s);
                    MessageBox.Show(s + " was mentioned in this post");
                }
            }
            return mentions;
        }
    }

    class SIR : Email
    {
        public string incidentReport;
        public string sortCode;
        
        public SIR()
        {
            incidentReport = null;
            sortCode = null;
        }

        public SIR(string nI, string nC)
        {
            incidentReport = nI;
            sortCode = nC;
        }


        public void setiReport(string Report)
        {
            incidentReport = Report;
        }

        public void setiCode(string Code)
        {
            sortCode = Code;
        }

             

    }
}
